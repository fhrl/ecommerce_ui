import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Product from './pages/Product'
import Cart from './pages/Cart'
import Home from './pages/Home'
import Login from './pages/Login'
import ProductList from './pages/ProductList'
import Register from './pages/Register'

const AppRouter = () => {
    return (
        <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/register">
          <Register />
        </Route>
        <Route path="/product">
          <Product />
        </Route>
        <Route path="/product-list">
          <ProductList />
        </Route>
        <Route path="/cart">
          <Cart />
        </Route>
        <Redirect from='/logout' to='/home' />
      </Switch>
    )
}

export default AppRouter
